# TD Pistus

ceci est un exemple de feature !

## Objectifs

L'objectif du TP est avant tout de faire du Python, pas de passer 1h sur du Git. Cette partie est complètement optionnelle et je laisse à votre appréciation d'utiliser cet outil.

## Fonctionnement

Vous ne pourrez pas push sur main. Faites vous une branche avec ```git switch -c <nom de la branche>``` sur laquelle vous pourrez faire des commits réguliers. Vous pouvez même créer d'autres branches pour les fusionner dans votre branche principale. Si vous souhaitez travailler en groupe à partir d'une même branche c'est possible.

## Rappels de Git

- Créer une branche et s'y déplacer:  ```git switch -c <nom de la branche>```
- Se déplacer sur une branche: ```git switch <nom de la branche>```
- Ajouter des fichiers à l'index: ```git add <chemin du fichier/dossier>```
- Créer un commit: ```git commit -m <message de commit>```
- Pull: ```git pull```
- Push: ```git push```
- Fusionner la branche Mabranche dans celle dans laquelle vous êtes: ```git merge MaBranche```
- Obtenir des informations utiles: ```git status```

## En cas de problème

N'hésitez pas à m'appeler et/ou à chercher sur internet, il y a très souvent la réponse.

